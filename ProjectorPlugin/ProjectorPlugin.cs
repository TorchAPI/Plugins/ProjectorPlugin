﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Windows.Controls;
using NLog;
using Sandbox.Engine.Multiplayer;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.GameSystems;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Torch;
using Torch.API;
using Torch.Managers;
using Torch.API.Managers;
using Torch.API.Plugins;
using Torch.API.Session;
using Torch.Views;
using VRage.Game;
using Torch.Managers.PatchManager;
using Torch.Managers.PatchManager.MSIL;
using Torch.Session;
using Torch.Utils;
using VRage.Utils;
using VRageMath;

namespace ProjectorPlugin
{
    public class ProjectorPlugin: TorchPluginBase, IWpfPlugin
    {
        //[Guid("4DD8A8F9-3B16-4FE2-8DFF-C6044554F308")]
        private PatchManager _pm;
        private PatchContext _context;
        private TorchSessionManager _sessionManager;
        private static Logger Log = LogManager.GetLogger("ProjectorPlugin");

        public static ProjectorPlugin Instance { get; private set; }
        
        public override void Init(ITorchBase torch)
        {
            base.Init(torch);
            Instance = this;
            PluginSettings.Instance.Load();
            _pm = torch.Managers.GetManager<PatchManager>();
            _context = _pm.AcquireContext();
            Patch(_context);
        }

        private UserControl _control;
        public UserControl Control => _control ?? (_control = new PropertyGrid() { DataContext = PluginSettings.Instance });

        public UserControl GetControl()
        {
            return Control;
        }

        public override void Dispose()
        {
            base.Dispose();
            //Log.Info("Shutting down plugin: {0} - {1}", Name, Version);
            _pm.FreeContext(_context);
        }

        public static void Patch(PatchContext ctx)
        {
            //ctx.GetPattern(typeof(MyProjectorBase).GetMethod("UpdateProjection", BindingFlags.Instance | BindingFlags.NonPublic)).Prefixes.Add(typeof(ProjectorPlugin).GetMethod(nameof(UpdateProjection), BindingFlags.Static | BindingFlags.NonPublic));
            //ctx.GetPattern(typeof(MyProjectorBase).GetMethod("UpdateAfterSimulation", BindingFlags.Instance | BindingFlags.Public)).Prefixes.Add(typeof(ProjectorPlugin).GetMethod(nameof(UpdateAfterSimulation), BindingFlags.Static | BindingFlags.NonPublic));
            //Log.Info("Patching done");

            var t = typeof(MyProjectorBase).GetNestedTypes(BindingFlags.NonPublic).First(c => c.Name == "MyProjectorUpdateWork");

            if(PluginSettings.Instance.PatchPerformance)
                ctx.GetPattern(t.GetMethod("OnComplete", BindingFlags.NonPublic | BindingFlags.Instance)).Transpilers.Add(typeof(ProjectorPlugin).GetMethod(nameof(RemoveAllForeach), BindingFlags.Static | BindingFlags.NonPublic));
            ctx.GetPattern(typeof(MyProjectorBase).GetMethod("previewGrid_OnBlockAdded", BindingFlags.NonPublic | BindingFlags.Instance)).Prefixes.Add(typeof(ProjectorPlugin).GetMethod(nameof(PreviewAddedPatch), BindingFlags.Static | BindingFlags.NonPublic));
        }

        private static bool UpdateProjection(MyProjectorBase __instance)
        {
            return false;
        }

        private static FieldInfo _clipboardField = typeof(MyProjectorBase).GetField("m_clipboard", BindingFlags.Instance | BindingFlags.NonPublic);

        private static void UpdateAfterSimulation(MyProjectorBase __instance)
        {
            var clipboard = (MyProjectorClipboard)_clipboardField.GetValue(__instance);
            if(clipboard.IsActive)
                clipboard.Deactivate();
            //Log.Info("Deactivating clipboard");
        }
        
        //Equinox magic
        private static IEnumerable<MsilInstruction> RemoveAllForeach(IEnumerable<MsilInstruction> input)
        {
            MsilLabel activeLabel = null;
            OpCode? prevOpcode = null;
            foreach (var k in input)
            {
                if (k.OpCode == OpCodes.Callvirt && (k.Operand is MsilOperandInline<MethodInfo> method) && method.Value.Name == "GetEnumerator")
                {
                    yield return new MsilInstruction(OpCodes.Call).InlineValue(true);
                    Debug.Assert(activeLabel == null);
                    activeLabel = new MsilLabel();
                    yield return new MsilInstruction(OpCodes.Brtrue).InlineTarget(activeLabel);
                }

                if (prevOpcode.HasValue && prevOpcode.Value == OpCodes.Endfinally)
                {
                    if (activeLabel != null)
                        k.LabelWith(activeLabel);
                    activeLabel = null;
                }
                prevOpcode = k.OpCode;
                yield return k;
            }
        }

        [ReflectedMethod(Name = "IsProjecting")]
        private static Func<MyProjectorBase, bool> _isProjectingGetter;

        // ReSharper disable once InconsistentNaming
        private static bool PreviewAddedPatch(MyProjectorBase __instance, MySlimBlock obj)
        {

            var CubeGrid = __instance?.CubeGrid;
            if (CubeGrid == null)
                return true;

            var ProjectedGrid = __instance.ProjectedGrid;

            if (PluginSettings.Instance.DetectError && (obj == null || (_isProjectingGetter(__instance) && ProjectedGrid == null)))
            {
                if (PluginSettings.Instance.DisableOnError)
                    __instance.Enabled = false;
                if (PluginSettings.Instance.LogError)
                    Log.Warn("Caught null reference in projector. KEEEEEEN");
                return false;
            }

            if (PluginSettings.Instance.PatchPerformance && ProjectedGrid != null)
            {
                Vector3I transformed = ProjectedGrid.WorldToGridInteger(CubeGrid.GridIntegerToWorld(obj.Position));
                if (ProjectedGrid.GetCubeBlock(transformed) == null)
                    return false;
            }

            return true;
        }
    }
}
