﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using NLog;
using Torch;
using Torch.Views;

namespace ProjectorPlugin
{
    [Serializable]
    public class PluginSettings : ViewModel
    {
        protected static Logger Log = LogManager.GetCurrentClassLogger();
        private static PluginSettings _instance;
        public static PluginSettings Instance => _instance ?? (_instance = new PluginSettings());

        #region Private Fields

        private bool _logError;
        private bool _disableOnError;
        private bool _detectError;
        private bool _patchPerformance;

        #endregion

        #region Public Properties

        [Display(Name = "Detect Error", Description = "Sets up logic to detect some null reference errors in the projector, and prevent them.")]
        public bool DetectError
        {
            get => _detectError;
            set => SetValue(ref _detectError, value);
        }

        [Display(Name="Disable on Error", Description = "Turns off projectors that cause null reference errors.")]
        public bool DisableOnError
        {
            get => _disableOnError;
            set => SetValue(ref _disableOnError, value);
        }

        [Display(Name = "Log Error", Description = "Prints to log when a projector throws an error.")]
        public bool LogError
        {
            get => _logError;
            set=>SetValue(ref _logError, value);
        }

        [Display(Name = "Patch Performance", Description = "Patches the projector code to increase performance. MUST RESTART TORCH FOR THIS TO TAKE EFFECT")]
        public bool PatchPerformance
        {
            get => _patchPerformance;
            set => SetValue(ref _patchPerformance, value);
        }

        #endregion

        public PluginSettings()
        {
            _patchPerformance = true;
            _disableOnError = true;
            _detectError = true;
            this.PropertyChanged += (sender, args) => Save();
        }

        #region Loading and Saving

        /// <summary>
        ///     Loads our settings
        /// </summary>
        public void Load()
        {
            try
            {
                string fileName = Path.Combine(ProjectorPlugin.Instance.StoragePath, "Projector-Settings.xml");
                if (File.Exists(fileName))
                {
                    using (var reader = new StreamReader(fileName))
                    {
                        XmlSerializer x = new XmlSerializer(typeof(PluginSettings));
                        var settings = (PluginSettings)x.Deserialize(reader);
                        reader.Close();

                        _instance = settings;
                    }
                }
                else
                {
                    Log.Info("No settings. Initialzing new file at " + fileName);
                    _instance = new PluginSettings();
                    using (var writer = new StreamWriter(fileName))
                    {
                        XmlSerializer x = new XmlSerializer(typeof(PluginSettings));
                        x.Serialize(writer, _instance);
                        writer.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        /// <summary>
        ///     Saves our settings
        /// </summary>
        public void Save()
        {
            Log.Info($"Saved");

            try
            {
                    string fileName = Path.Combine(ProjectorPlugin.Instance.StoragePath, "Projector-Settings.xml");
                using (var writer = new StreamWriter(fileName))
                {
                    XmlSerializer x = new XmlSerializer(typeof(PluginSettings));
                    x.Serialize(writer, _instance);
                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        #endregion

    }
}
